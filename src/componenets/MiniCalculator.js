import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      result: 0
    };

    this.add = this.add.bind(this);
    this.minus = this.minus.bind(this);
    this.multi = this.multi.bind(this);
    this.divise = this.divise.bind(this);
  }

  add() {
    this.setState({result: this.state.result + 1})
  }

  render() {
    return (
        <section>
          <div>计算结果为:
            <span className="result"> {this.state.result} </span>
          </div>
          <div className="operations">
            <button onClick={this.add}>加1</button>
            <button onClick={this.minus}>减1</button>
            <button onClick={this.multi}>乘以2</button>
            <button onClick={this.divise}>除以2</button>
          </div>
        </section>
    );
  }

  minus() {
    this.setState({result: this.state.result - 1})
  }

  multi() {
    this.setState({result: this.state.result * 2})
  }

  divise() {
    this.setState({result: this.state.result / 2})
  }
}

export default MiniCalculator;

